package com.devcamp.j02_javabasic.s30;

public class Customer {

    byte myByte; // byte
    short myShortNumber; // short
    long myLongNumber; // long
    double myDouberNumber; // double
    float myFloatNumber; // float
    int myIntNumber; // floating oint number
    char myLetter; // Character
    boolean myBool; // boolean

    // Create method
    public Customer() {
        myByte = 127;
        myShortNumber = 2022;
        myLongNumber = 2000;
        myDouberNumber = 123.4;
        myIntNumber = 5;
        myFloatNumber = 9.99f;
        myLetter = 'P';
        myBool = false; // boolean
    }

    public Customer(byte byteNum, short shortNum, long longNum, double doubleNum, int intNum, float floatNum,
            char letter, boolean bool) {
        myByte = byteNum;
        myShortNumber = shortNum;
        myDouberNumber = doubleNum;
        myIntNumber = intNum;
        myFloatNumber = floatNum;
        myLetter = letter;
        myBool = bool;
    }

    public static void main(String[] args) {
        Customer customer = new Customer();
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDouberNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 1: ");
        byte myByte1 = 10;
        short myShort1 = 2001;
        customer = new Customer(myByte1, myShort1, 7001, 671.9, 231, 71.99f, 'B', false );
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDouberNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);

        System.out.println("Run 2: ");
        byte myByte2 = 20;
        short myShort2 = 2002;
        customer = new Customer(myByte2, myShort2, 7002, 672.9, 232, 72.99f, 'B', false );
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDouberNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);

        System.out.println("Run 3: ");
        byte myByte3 = 30;
        short myShort3 = 3002;
        customer = new Customer(myByte3, myShort3, 7003, 673.9, 233, 73.99f, 'B', false );
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDouberNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);

        System.out.println("Run 4: ");
        byte myByte4 = 40;
        short myShort4 = 4002;
        customer = new Customer(myByte4, myShort4, 7004, 674.9, 234, 74.99f, 'B', false );
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDouberNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);

        System.out.println("Run 5: ");
        byte myByte5 = 50;
        short myShort5 = 5002;
        customer = new Customer(myByte5, myShort5, 7005, 675.9, 235, 75.99f, 'B', false );
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDouberNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
    }

}
